import json
import socket
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.has_boost = False

        self.track = None
        self.last_track_piece = None
        self.track_piece_index = 0
        self.longest_straight = None

        self.last_tick_distance = 0 
        self.current_velocity = 0

        self.current_throttle = 0

        self.current_lane = 0

        self.has_crashed = False

		#track : ~max const throttle
        #keimola: .6
        #germany: .4
        #usa: .5
        #france: .4
        self.max_const_throttle = .6

    def msg(self, msg_type, data):
       self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def createRace(self, trackname, carcount):
        return self.msg("createRace", { "trackName": trackname, "carCount": carcount, "botId": {"name":self.name, "key":self.key} })

    def throttle(self, throttle):
        self.current_throttle = throttle
        self.msg("throttle", throttle)

    def increase_throttle(self, val):
        self.throttle(self.current_throttle - val)

    def decrease_throttle(self, val):
        self.throttle(self.current_throttle + val)

    def turbo(self):
        if(self.has_boost):
            print('Using turbo')
            self.msg("turbo", data="turbo")
            self.has_boost = False
        else:
            print('Tried to use turbo, but none available.')

    def switch(self, side):
        if(side == 'Left'):
            self.current_lane 
        self.msg('switchLane', data=side)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        #createRace breaks CI...
        #self.createRace()

        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_turbo_available(self, data):
        print('Got turbo')
        if(not self.has_crashed):
	        self.has_boost = True

    def on_game_init(self, data):
        print("GameInit")

        self.track = data['race']['track']['pieces']
        self.track_piece_index = 0

        longest_straight = 0
        current_straight = 0

        current_straight_index = 0
        longest_straight_index = 0

        index = 0

        #Grab starting indexes of the longest straights for getting max boost
        #Track is a loop, so double it up to find the longest straights
        for piece in self.track+self.track:
            if not 'radius' in piece:
                if current_straight == 0:
                    current_straight_index = index

                current_straight += piece['length']

            else:
                if(current_straight > longest_straight):
                    longest_straight = current_straight
                    longest_straight_index = current_straight_index

                current_straight = 0
            index +=1

        self.longest_straight_index = longest_straight_index

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        my_car = [car for car in data if car['id']['name'] == 'sb_admin'][0]

        track_piece_index = my_car['piecePosition']['pieceIndex']
        self.current_lane = my_car['piecePosition']['lane']['endLaneIndex']

        track_piece = self.track[track_piece_index]

        #TODO - lane offset
        if 'switch' in track_piece and track_piece['switch']:
            len_left_turns = 0
            len_right_turns = 0
            for piece in self.track[track_piece_index+1:]:
                if('switch' in piece): #Tidy...
                    break
                else:
                    if('radius' in piece and piece['angle'] < 0):
                        len_left_turns += (math.radians(abs(piece['angle']))) * piece['radius']
                    elif('radius' in piece and piece['angle'] > 0):
                        len_right_turns += (math.radians(abs(piece['angle']))) * piece['radius']

            if(len_right_turns > len_left_turns):
                self.msg('switchLane', data='Left')
            else:
                self.msg('switchLane', data='Right')

        #Turbo takes priority over throttle - both consume a game tick
        if track_piece_index == self.longest_straight_index and self.has_boost:
            self.turbo()

        #Default to constant throttle...
        self.throttle(self.max_const_throttle)

        if(self.track_piece_index != track_piece_index):
            self.track_piece_index = track_piece_index

    def on_crash(self, data):
        print('Crashed')
        self.has_crashed = True
        self.ping()

    def on_spawn(self, data):
    	print('Back on track!')
        self.has_crashed = False
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable':self.on_turbo_available,
            'spawn':self.on_spawn
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
